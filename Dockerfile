FROM alpine:latest

RUN apk update
RUN apk upgrade

RUN apk add python3 py3-pip
RUN mkdir -p /usr/src/my-app
WORKDIR /usr/src/my-app
COPY start.py requirements.txt /usr/src/my-app/
ADD handlers /usr/src/my-app/handlers
ADD templates /usr/src/my-app/templates
RUN pip install -r requirements.txt
EXPOSE 5000
ENV TOKEN=your_token

CMD ["python3", "start.py"]